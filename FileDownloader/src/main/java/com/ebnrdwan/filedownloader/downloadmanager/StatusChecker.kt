package com.ebnrdwan.filedownloader.downloadmanager

import android.app.DownloadManager
import android.content.Context
import android.database.Cursor
import com.ebnrdwan.filedownloader.errorhandler.ErrorHandler
import com.ebnrdwan.filedownloader.interactions.DownloadStatusCallback
import com.ebnrdwan.filedownloader.models.DownloadType
import com.ebnrdwan.filedownloader.utils.Constants
import com.ebnrdwan.filedownloader.utils.FileUtils
import java.net.URI

/**
 * Checking status of a certain download process
 * @param context Application/Activity context
 * @param downloadID Current downloading process id
 * @param onCompleteListener internal Callback for [com.ebnrdwan.filedownloader.downloadmanager.DownloadManager] to
 *  notify it if only download completed successfully or by error
 * @param downloadStatusCallback  public Callback for library consumer to notify it
 *  with all downloading states [pending-onProgress-error-completed]
 * */
internal class StatusChecker(
    val context: Context,
    private val downloadID: Long,
    private val onCompleteListener: OnCompleteListener,
    private val downloadStatusCallback: DownloadStatusCallback? = null
) {
    private var previousStatus = Constants.NO_STATUS
    var query: DownloadManager.Query = DownloadManager.Query()
    private var downloadManager: DownloadManager

    init {
        query.setFilterById(downloadID)
        downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
    }

    /*Start status checking*/
    fun checkStatus() {
        val cursor: Cursor = downloadManager.query(query)
        queryDownloadStatus(cursor)
    }

    /**Query download status from provided data
     * @param cursor Representing provided data from current download process
     * */
    private fun queryDownloadStatus(cursor: Cursor) {
        if (cursor.moveToFirst()) {
            val status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))

            when (status) {

                DownloadManager.STATUS_PENDING -> {
                    if (previousStatus != DownloadManager.STATUS_PENDING) {
                        onDownloadPending()
                    }
                }
                DownloadManager.STATUS_PAUSED -> {
                    if (previousStatus != DownloadManager.STATUS_PAUSED) {
                        onDownloadPause(cursor)
                    }
                }
                DownloadManager.STATUS_RUNNING -> {
                    onDownloadRunning(cursor)
                }
                DownloadManager.STATUS_SUCCESSFUL -> {
                    if (previousStatus != DownloadManager.STATUS_PAUSED) {
                        onDownloadSuccess(cursor)
                    }

                }
                DownloadManager.STATUS_FAILED -> {
                    if (previousStatus != DownloadManager.STATUS_FAILED) {
                        onDownloadFailed(cursor)
                    }

                }
            }
        }
    }

    /** Extract pending status and pass it to status callback
     * */
    private fun onDownloadPending() {
        downloadStatusCallback?.pending(downloadID)
        previousStatus = DownloadManager.STATUS_PENDING
    }


    /** Extract paused status and pass it to status callback
     * */
    private fun onDownloadPause(cursor: Cursor) {
        val reason = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON))
        downloadStatusCallback?.onPause(downloadID,reason)
        previousStatus = DownloadManager.STATUS_PAUSED
    }


    /** Extract failed status and pass it to status callback
     * @param cursor Representing download database */
    private fun onDownloadFailed(cursor: Cursor) {
        val reason = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON))
        downloadStatusCallback?.onError(
            downloadID,
            ErrorHandler.getErrorTypeBy(reason, DownloadType.DOWNLOAD_MANAGER)
        )
        onCompleteListener.onError(downloadID,getFileUri(cursor))
        previousStatus = DownloadManager.STATUS_FAILED
    }


    /** Extract downloaded file path from downloading database
     * @param cursor Representing download database */
    private fun getFileUri(cursor: Cursor) =
        URI(cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI))).path


    /** Extract success status and pass it to status callback
     * @param cursor Representing download database */
    private fun onDownloadSuccess(cursor: Cursor) {
        val uri = URI(getFileUri(cursor));
        val path = uri.path
        downloadStatusCallback?.onCompleted(downloadID, FileUtils.getFileUri(context, path).toString(), uri.path)
        onCompleteListener.onComplete(downloadID)
        previousStatus = DownloadManager.STATUS_SUCCESSFUL
    }


    /** Extract running status and pass it to status callback
     * @param cursor Representing download database */
    private fun onDownloadRunning(cursor: Cursor) {
        val progress: Long =
            cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR))
        val totalByteCount: Long =
            cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES))
        downloadStatusCallback?.onProgress(downloadID, progress, totalByteCount)
        previousStatus = DownloadManager.STATUS_RUNNING
    }

}