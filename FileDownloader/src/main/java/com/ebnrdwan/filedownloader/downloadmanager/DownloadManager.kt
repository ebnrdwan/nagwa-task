package com.ebnrdwan.filedownloader.downloadmanager

import android.Manifest
import android.app.DownloadManager
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import androidx.core.content.ContextCompat
import com.ebnrdwan.filedownloader.factory.IDownloadFactory
import com.ebnrdwan.filedownloader.interactions.DownloadStatusCallback
import com.ebnrdwan.filedownloader.models.ErrorMessages
import com.ebnrdwan.filedownloader.models.HttpType
import com.ebnrdwan.filedownloader.models.StorageType
import com.ebnrdwan.filedownloader.utils.Constants
import com.ebnrdwan.filedownloader.utils.FileUtils
import com.ebnrdwan.filedownloader.utils.LogUtils
import com.ebnrdwan.filedownloader.utils.OsVersionUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.util.concurrent.TimeUnit


/**
 * Used to download files with Android Download Manager
 * [DownloadManager] is limited to download files in external storage
 * @param context Application/Activity context
 * */
internal class DownloadManager(
    val context: Context
) : IDownloadFactory {
    private val compositeDisposablesMap: MutableMap<Long, CompositeDisposable> = mutableMapOf()
    private var downloadStatusCallbackMap: MutableMap<Long, DownloadStatusCallback?> = mutableMapOf()

    /**
     * this is where the download process happens, first it builds download request then pass it to download manager to start the downloading process
     *  if status callback is sent this function will initialize status checker object to send download status callbacks to consumer class
     * @param downloadUrl Api for downloadable file
     * @param httpType HTTP method [:GET],[:POST] in download http request, [HttpType.GET] is the default value if not sent
     * @param storageType Main storage where the downloaded file will be saved
     * @param syncWithGallery Syncing media files with Gallery
     * @param folderName Folder where the downloaded file will be saved, if not sent will be saved in a corresponding directory based on file mimeType
     * @param fileName Name of downloaded file, value will be extracted from [downloadUrl] if [fileName] is not sent. if [downloadUrl] also doesn't contain
     *  file name, then unique file name will be generated using DownloadedFile as prefix
     * @param mimeType Type of downloaded file,value will be extracted from [downloadUrl] if [mimeType] not sent
     * @param headers  Are sent to the download manager request, will be ignored if not sent
     * @param showNotificationStatus Pass false if you need to handle downloading process callback ui by yourself, like showing ProgressDialog or
     *  custom Notification when download is running
     * @param downloadStatusCallback Optional call back with the downloading status
     * @throws RuntimeException If needed permissions not handled or granted by user
     * @throws RuntimeException If storage type in not external
     * @throws RuntimeException If APi method is post
     * @return Unique id of current downloading process downloadId*/
    override fun download(
        downloadUrl: String,
        httpType: HttpType,
        storageType: StorageType,
        syncWithGallery: Boolean?,
        folderName: String?,
        fileName: String?,
        mimeType: String?,
        headers: Map<String, Any>?,
        showNotificationStatus: Boolean?,
        body: Any?,
        downloadStatusCallback: DownloadStatusCallback?
    ): Long? {
        if (!isStoragePermissionsGranted(context) && storageType == StorageType.ExternalSharedFiles && !OsVersionUtils.isAndroidQOrMore())
            LogUtils.logError(
                throwable = Exception(ErrorMessages.WRITE_STORAGE_PERMISSION_NOT_GRANTED)
            )


        if (storageType == StorageType.InternalAppSpecificFiles || storageType == StorageType.InternalAppSpecificCache) {
            LogUtils.logError(throwable = RuntimeException(ErrorMessages.INVALID_STORAGE_TYPE))
            return null
        }
        if (storageType == StorageType.ExternalAppSpecificCache) {
            LogUtils.logError(throwable = RuntimeException(ErrorMessages.EXTERNAL_CACHE_NOT_ALLOWED))
            return null
        }
        if (httpType == HttpType.POST) {
            LogUtils.logError(throwable = RuntimeException(ErrorMessages.INVALID_HTTP_TYPE))
            return null
        }

        val request = buildDownloadManagerRequest(
            downloadUrl,
            folderName,
            storageType,
            fileName,
            mimeType,
            showNotificationStatus,
            headers
        )
        val downloadId = startDownloading(request)
        this.downloadStatusCallbackMap[downloadId] = downloadStatusCallback
        downloadStatusCallback?.let { bindStatusChecking(downloadId, downloadStatusCallback) }

        return downloadId
    }

    /** this function to validate and handle the file name, it tries to extract it from [downloadUrl], if failed
     * it will create a unique file name based on mime type
     * @param fileName Name of downloaded file, value will be extracted from [downloadUrl] if [fileName] is not sent. if [downloadUrl] also doesn't contain
     *  file name, then unique file name will be generated using DownloadedFile as prefix
     * @param downloadUrl Api for downloadable file
     * @param mimeType Type of downloaded file,value will be extracted from [downloadUrl] if [mimeType] not sent
     */
    private fun checkAndHandleFileName(
        fileName: String?,
        downloadUrl: String,
        mimeType: String?
    ): String? {
        var validFileName = fileName

        if (fileName.isNullOrEmpty())
            validFileName = FileUtils.getFileNameFromURL(downloadUrl)

        if (validFileName.isNullOrEmpty()) {
            validFileName = FileUtils.generateUniqueFileName(Constants.DefaultPrefixFileName, mimeType)
        }
        return validFileName
    }

    /** this function to validate mimeType, if [mimeType] is not sent it will try to extract it from the [downloadUrl]
     * @param mimeType Type of downloaded file,value will be extracted from [downloadUrl] if [mimeType] not sent
     * @param downloadUrl Api for downloadable file
     */
    private fun checkAndHandleMimeType(
        mimeType: String?,
        downloadUrl: String
    ) = if (mimeType.isNullOrEmpty())
        FileUtils.getMimeTypeFromUrl(downloadUrl) else mimeType

    /**
     * Builds Download manager request out of downloadParams
     *  @param downloadUrl Api for downloadable file
     * @param folderName Folder where the downloaded file will be saved
     * @param storageType Main storage where the downloaded file will be saved
     * @param _fileName Name of downloaded file, value will be extracted from[downloadUrl] if it contains file name
     * @param _mimeType Extension of downloaded file,value will be extracted from[downloadUrl] if it contains file extension
     * @param showNotificationStatus Pass false if you need to handle downloading process callback ui by yourself, like showing ProgressDialog or
     * @param headers  Are sent to the download manager request, will be ignored if not sent
     * @return Download manager request [DownloadManager.Request]*/
    private fun buildDownloadManagerRequest(
        downloadUrl: String,
        folderName: String?,
        storageType: StorageType,
        _fileName: String?,
        _mimeType: String?,
        showNotificationStatus: Boolean?,
        headers: Map<String, Any>?
    ): DownloadManager.Request? {
        val mimeType = checkAndHandleMimeType(_mimeType, downloadUrl)
        val fileName = checkAndHandleFileName(_fileName, downloadUrl, mimeType)
        val request = DownloadManager.Request(Uri.parse(downloadUrl))
            .setTitle(fileName)
            .setMimeType(mimeType)
            .setNotificationVisibility(
                if (showNotificationStatus == false) DownloadManager.Request.VISIBILITY_HIDDEN else
                    DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED
            )
            .setAllowedOverMetered(true)
            .setAllowedOverRoaming(true)
        if (headers != null) {
            for (header in headers) {
                request.addRequestHeader(header.key, header.value.toString())
            }
        }
        return setDownloadDestinationUri(
            request,
            context,
            folderName,
            storageType,
            fileName,
            mimeType
        )
    }


    /** Set destination uri based on [StorageType] inside the download request
     * @param context Application/Activity context
     * @param folderName Folder where the downloaded file will be saved
     * @param storageType Main storage where the downloaded file will be saved
     * @param fileName Name of downloaded file, value will be extracted from downloadUrl if it contains file name
     * @param mimeType Extension of downloaded file,value will be extracted from downloadUrl if it contains file extension
     * @return [DownloadManager.Request]
     * */
    private fun setDownloadDestinationUri(
        request: DownloadManager.Request,
        context: Context,
        folderName: String?,
        storageType: StorageType,
        fileName: String?,
        mimeType: String?
    ): DownloadManager.Request {

        if (storageType == StorageType.ExternalAppSpecificFiles) {
            request.setDestinationInExternalFilesDir(
                context,
                folderName,
                fileName
            )
        } else {
            request.setDestinationUri(
                Uri.fromFile(
                    File(
                        FileUtils.createFileUri(
                            context,
                            storageType,
                            folderName,
                            fileName,
                            mimeType
                        )
                    )
                )
            )
        }
        return request
    }

    /**
     * Start downloading process by [downloadManagerRequest]
     *  @param downloadManagerRequest The download manager request that should be passed to download manager to build download process
     * @return Download id*/
    private fun startDownloading(
        downloadManagerRequest: DownloadManager.Request?
    ): Long {
        val downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        return downloadManager.enqueue(downloadManagerRequest)
    }


    /**
     * Query status of downloading process by [downloadId]
     *  @param downloadId Id of downloading process
     * @param downloadStatusCallback Optional call back with the downloading status
     * */
    private fun bindStatusChecking(
        downloadId: Long,
        downloadStatusCallback: DownloadStatusCallback?
    ) {
        val statusChecker = StatusChecker(
            context,
            downloadId,
            completeListener,
            downloadStatusCallback
        )

        if (compositeDisposablesMap[downloadId] == null) {
            compositeDisposablesMap[downloadId] = CompositeDisposable()
        }

        compositeDisposablesMap[downloadId]?.add(
            Observable.interval(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    statusChecker.checkStatus()
                }, { LogUtils.logError(throwable = it) })
        )
    }


    private val completeListener = object : OnCompleteListener {
        /**
         * Download completed successfully
         */
        override fun onComplete(downloadId: Long) {
            compositeDisposablesMap[downloadId]?.dispose()
        }

        /**
         * Download completed by error
         * @param filePath path for error file
         */
        override fun onError(downloadId: Long, filePath: String?) {
            compositeDisposablesMap[downloadId]?.dispose()
            filePath?.let { FileUtils.deleteFile(it) }
        }
    }


    /**
     * Handle cancel downloading action
     * @param downloadID Id of download process needed to be canceled
     * @return Number of deleted rows*/
    override fun cancel(downloadID: Long): Int {
        val downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        val removedDownloads = downloadManager.remove(downloadID)
        if (removedDownloads > 0) downloadStatusCallbackMap[downloadID]?.onCanceled(downloadID)
        return removedDownloads
    }


    /**Check if [:WRITE_EXTERNAL_STORAGE] permission is granted
     * @return IsGranted [Boolean]*/
    private fun isStoragePermissionsGranted(context: Context): Boolean {
        val storagePermission = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        for (permission: String in storagePermission) {
            if (ContextCompat.checkSelfPermission(
                    context,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) return false
        }
        return true
    }


}