package com.ebnrdwan.filedownloader.downloadmanager

/**
 * Interface to get call back if download completed successfully or by error
 * */
internal interface OnCompleteListener {
    /**
     * Download completed successfully
     * @param downloadId Current downloading process id
     */
    fun onComplete(downloadId: Long)

    /**
     * Download completed by error
     * @param downloadId Current downloading process id
     * @param filePath uri of the downloaded file if existed
     */
    fun onError(downloadId: Long, filePath: String?)
}