


package com.ebnrdwan.filedownloader.workmanager

import android.content.Context
import com.ebnrdwan.filedownloader.factory.IDownloadFactory
import com.ebnrdwan.filedownloader.interactions.DownloadStatusCallback
import com.ebnrdwan.filedownloader.models.HttpType
import com.ebnrdwan.filedownloader.models.StorageType

class WorkManager(
    val context: Context,
) : IDownloadFactory {
    override fun download(
        downloadUrl: String,
        httpType: HttpType,
        storageType: StorageType,
        syncWithGallery: Boolean?,
        folderName: String?,
        fileName: String?,
        mimeType: String?,
        headers: Map<String, Any>?,
        showNotificationStatus: Boolean?,
        body: Any?,
        downloadStatusCallback: DownloadStatusCallback?
    ): Long? {
        TODO("Not yet implemented")
    }


    override fun cancel(downloadID: Long): Int {
        TODO("Not yet implemented")
    }
}