package com.ebnrdwan.filedownloader.interactions

import com.ebnrdwan.filedownloader.errorhandler.IDownloadError

/*Callback for downloading status*/
interface DownloadStatusCallback {

    /**
     * Fires if download is pending for waiting internet connection
     * @param downloadId Current downloading process id*/
    fun pending(downloadId: Long)

    /** Fires in case of downloading started and running
     * @param downloadId Current downloading process id
     * @param downloadedBytes Currently filed bytes in downloading file
     * @param totalBytes Total bytes that needed to be downloaded to the file
     * */
    fun onProgress(downloadId: Long, downloadedBytes: Long, totalBytes: Long)

    /**
     * Fires if downloading paused for any reason
     * @param downloadId Current downloading process id
     * @param reasonCode pause reason code, there are pause reason codes in download manager and pause reason codes in work manager
     * @see <a href="Download manager reasons">https://developer.android.com/reference/android/app/DownloadManager#PAUSED_QUEUED_FOR_WIFI</a> for Download manager reasons
     * For work manager reasons, you can find them defined in work manager module
     *  */
    fun onPause(downloadId: Long, reasonCode: Int)

    /**
     * Fires if downloading onCompleted successfully
     * @param downloadId Current downloading process id
     * @param contentPath The file uri created by file provider so that other apps can access and open the file
     * @param absolutePath The absolute file path which is private to the app and not accessible by other apps */
    fun onCompleted(downloadId: Long, contentPath: String, absolutePath: String)

    /**
     * Fires if downloading canceled for any reason
     * @param downloadId Current downloading process id*/
    fun onCanceled(downloadId: Long)

    /**
     * Fires if downloading Completed by error
     * @param downloadId Current downloading process id
     * @param downloadManagerError Error constant to check and present proper message to user
     * */
    fun onError(downloadId: Long, downloadManagerError: IDownloadError)
}