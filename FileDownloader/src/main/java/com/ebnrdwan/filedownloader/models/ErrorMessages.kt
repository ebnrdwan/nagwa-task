package com.ebnrdwan.filedownloader.models

object ErrorMessages {
    const val INVALID_HTTP_TYPE = "Download Manager supports only GET HTTP calls, to support POST types, please use the Work Manager"
    const val INVALID_STORAGE_TYPE = "Download Manager can't access internal storage, to save in internal storage please use Work Manager"
    const val WRITE_STORAGE_PERMISSION_NOT_GRANTED = "android.permission.WRITE_EXTERNAL_STORAGE permission is required, please handle permissions in your app"
    const val EXTERNAL_CACHE_NOT_ALLOWED = "Download Manager can't save in external cache, to save in external storage please use Work Manager"
}