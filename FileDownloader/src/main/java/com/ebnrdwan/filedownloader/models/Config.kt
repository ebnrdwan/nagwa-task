


package com.ebnrdwan.filedownloader.models
/**
 * Containing optional configuration to download process
 * @param showNotificationStatus Pass false if you need to handle downloading process callback ui by yourself, like showing [ProgressDialog] or
 *  custom[Notification] when download is running */
data class Config(val showNotificationStatus:Boolean)