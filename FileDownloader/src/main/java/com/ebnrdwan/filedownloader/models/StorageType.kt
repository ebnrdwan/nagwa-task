



package com.ebnrdwan.filedownloader.models

/**
 * Storage types that could be used to download files in*/
enum class StorageType {

    /**Non-persistent type that could be deleted if os need to free up space
     *
     * Hidden and private to the level that will not be located by your file manager
     *
     * It's encrypted from [Andorid.Q] and above
     *
     * Not available with [DownloadManagerComponent] */
     InternalAppSpecificCache,

    /**Persistent type that will not be deleted if os need to free up space
     *
     * Hidden and private to the level that will not be located by your file manager
     *
     * It's encrypted from [Andorid.Q] and above
     *
     * Not available with [DownloadManagerComponent] */
     InternalAppSpecificFiles ,

    /**Persistent type that will not be deleted if os need to free up space
     *
     *  App specific storage, to share it's files use [FileProvider]
     * */
    ExternalAppSpecificFiles ,


    /** Non-persistent type that could be deleted if os need to free up space
     *
     * App specific external storage
     *
     * Sharing files will be by [FileProvider]
     *
     * Accessible by fileManager*/
     ExternalAppSpecificCache ,

    /**Persistent type that will not be deleted if os need to free up space
     *
     * Files to be shared with other apps and not be deleted if app deleted
     *
     * [WRITE_EXTERNAL_STORAGE] Permission must be granted
     * */
     ExternalSharedFiles
}
