



package com.ebnrdwan.filedownloader.models

/**Represents the type of the download component
 * all newly added types must be added here*/
enum class DownloadType {
    DOWNLOAD_MANAGER,
    WORK_MANAGER
}