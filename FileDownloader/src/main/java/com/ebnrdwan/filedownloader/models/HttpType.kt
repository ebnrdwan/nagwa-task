


package com.ebnrdwan.filedownloader.models
/**
 * The HTTP method Type*/
enum class HttpType {
    GET,
    POST
}