

package com.ebnrdwan.filedownloader.utils

import android.os.Build


internal object OsVersionUtils {

    /**
     * Check if current device os version is >= Api 29
     * @return [Boolean]*/
    fun isAndroidQOrMore(): Boolean =
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q

}