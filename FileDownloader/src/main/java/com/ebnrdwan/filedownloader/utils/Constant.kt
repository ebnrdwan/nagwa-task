


package com.ebnrdwan.filedownloader.utils

object Constants {

    /** If no file name provided, [DefaultPrefixFileName]  will be used*/
    const val DefaultPrefixFileName: String = "DownloadedFile"

    const val NO_STATUS: Int = -1

}

