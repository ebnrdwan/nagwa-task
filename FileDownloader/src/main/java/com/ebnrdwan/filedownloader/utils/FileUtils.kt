package com.ebnrdwan.filedownloader.utils

import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.webkit.MimeTypeMap
import androidx.core.content.FileProvider
import com.ebnrdwan.filedownloader.models.StorageType
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


internal object FileUtils {
    private const val DATE_PATTERN = "yyyyMMdd_HHmmss"

    /**
     * Check if url contain extension and Extract file name from it
     * @param url Download url if not passed the function will return null*/
    internal fun getFileNameFromURL(url: String?): String? {
        return if (getMimeTypeFromUrl(url) != null)
            url?.substring(url.lastIndexOf("/") + 1)
        else null
    }


    /**
     * extract mime type from a url, if failed it will @return null
     * @param url Download url if not passed the function will return null */
    internal fun getMimeTypeFromUrl(url: String?): String? {
        return try {
            val ext = url?.substring(url.lastIndexOf(".") + 1)
            val mime = MimeTypeMap.getSingleton()
            mime.getMimeTypeFromExtension(ext)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }

    }


    /** Delete file
     * @param uri File path*/
    internal fun deleteFile(uri: String) {
        val file = File(uri)
        if (file.exists()) {
            file.delete()
        }
    }


    /**Get path of storage based on params
     * @param context
     * @param storageType To parse it's corresponding path
     * @param folderName To append it to [storageType] path
     * */
    private fun getDirectory(
        context: Context,
        storageType: StorageType,
        folderName: String?
    ): String {
        val folder = File(getPathOfStorageType(context, storageType) + "/" + folderName)
        folder.mkdirs()
        return folder.path
    }

    /**Get path of storage based on params
     * @param context Application/Activity context
     * @param storageType To parse it's corresponding path
     *@return [Storage Path]
     * */
    private fun getPathOfStorageType(context: Context, storageType: StorageType): String? {
        return when (storageType) {
            StorageType.InternalAppSpecificCache -> context.cacheDir.absolutePath
            StorageType.InternalAppSpecificFiles -> context.filesDir.absolutePath
            StorageType.ExternalAppSpecificCache -> context.externalCacheDir?.absolutePath
            StorageType.ExternalAppSpecificFiles -> context.getExternalFilesDir(null)?.absolutePath
            StorageType.ExternalSharedFiles -> if (OsVersionUtils.isAndroidQOrMore()) context.getExternalFilesDir(
                null
            )?.absolutePath else Environment.getExternalStorageDirectory().absolutePath
        }
    }

    /**create path for a file with the following params
     * @param context Application/Activity context
     * @param storageType TO parse it's corresponding path
     * @param folderName TO append it to [storageType] path
     * @param fileName TO append it to [storageType] path
     * @param mimeType TO append it to [storageType] path
     * @return File path[String]*/
    fun createFileUri(
        context: Context,
        storageType: StorageType,
        folderName: String?,
        fileName: String?,
        mimeType: String?
    ): String {
        return if (getMimeTypeFromUrl(fileName) != null) {
            "${getDirectory(context, storageType, folderName)}/${fileName}"
        } else
            "${getDirectory(context, storageType, folderName)}/${fileName}.${mimeType}"
    }


    /**
     * create a unique file name using given prefix and current date.
     * @param prefix file name prefix
     */
    fun generateUniqueFileName(prefix: String, mimeType: String?): String {
        return prefix + SimpleDateFormat(DATE_PATTERN, Locale.ENGLISH).format(Date())
            .plus(".${mimeType}")
    }

    /**
     * get the content path created by file provider so that other apps can open this file
     * @param context Application/Activity context
     * @param fileUri The path for a file on the device storage */
    fun getFileUri(context: Context, fileUri: String): Uri {
        return try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                FileProvider.getUriForFile(context, context.packageName + ".provider", File(fileUri))
            } else {
                Uri.fromFile(File(fileUri))
            }
        } catch (e: Exception) {
            Uri.fromFile(File(fileUri))
        }
    }


}