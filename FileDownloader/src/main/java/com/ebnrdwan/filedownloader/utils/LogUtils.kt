package com.ebnrdwan.filedownloader.utils

import android.util.Log
import com.ebnrdwan.filedownloader.BuildConfig

object LogUtils {

    /** If no logging tag provided, [MODULE_TAG]  will be used*/
    const val MODULE_TAG = "FileDownloader"


    internal fun printStackTrace(exception: Exception) {
        exception.printStackTrace()
    }

    internal fun printStackTrace(throwable: Throwable) {
        throwable.printStackTrace()

    }

    /**
     * Log message in debug mode
     * @param TAG Log tag
     * @param msg That printed to log
     * */
    internal fun logDebug(TAG: String = MODULE_TAG, msg: String) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, msg)
        }
    }

    /**
     * Log errors and it's stack trace
     * @param TAG Log tag
     * @param msg That printed to log
     * @param throwable Error if found to be printed to stack trace
     * */
    internal fun logError(
        TAG: String = MODULE_TAG,
        msg: String? = null,
        throwable: Throwable? = null
    ) {
        msg?.let { Log.e(TAG, it) }
        if (throwable != null) {
            printStackTrace(
                throwable
            )
            throw throwable
        }
    }
}