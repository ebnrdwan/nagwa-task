package com.ebnrdwan.filedownloader.factory

import android.content.Context
import com.ebnrdwan.filedownloader.downloadmanager.DownloadManager
import com.ebnrdwan.filedownloader.models.DownloadType
import com.ebnrdwan.filedownloader.workmanager.WorkManager

/**
 * Responsible for providing the needed instance from downloading components by calling [getInstance] which is corresponding to [downloadComponent], [downloadParams] input params
 * @param context Application/Activity context
 * */
class DownloadFactory(
    val context: Context
) {


    /**
     * Provide instance of download component
     * @param [downloadType] Define which type of components to use in downloading process
     * @return [IDownloadFactory]*/
    fun getInstance(downloadType: DownloadType): IDownloadFactory? {

        return when (downloadType) {
            DownloadType.DOWNLOAD_MANAGER -> {
                return DownloadManager(context)
            }
            DownloadType.WORK_MANAGER -> {
                WorkManager(context)
            }
        }
    }

}