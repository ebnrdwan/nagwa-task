package com.ebnrdwan.filedownloader.factory

import com.ebnrdwan.filedownloader.interactions.DownloadStatusCallback
import com.ebnrdwan.filedownloader.models.HttpType
import com.ebnrdwan.filedownloader.models.StorageType

/**
 *Contract that every download component should implement
 */
interface IDownloadFactory {


    /**
     *Perform the download request and handling it's results
     *
     * this is where the download process happens, first it builds download request then pass it to download manager to start the downloading process
     *  if status callback is sent this function will initialize status checker object to send download status callbacks to consumer class
     * @param downloadUrl Api for downloadable file
     * @param httpType HTTP method [GET],[POST] in download http request, [HttpType.GET] is the default value if not sent
     * @param storageType Main storage where the downloaded file will be saved
     * @param syncWithGallery Syncing media files with Gallery
     * @param folderName Folder where the downloaded file will be saved, if not sent will be saved in a corresponding directory based on file mimeType
     * @param fileName Name of downloaded file, value will be extracted from [downloadUrl] if [fileName] is not sent
     * @param mimeType Type of downloaded file,value will be extracted from [downloadUrl] if [mimeType] not sent
     * @param headers  Are sent to the download manager request, will be ignored if not sent
     * @param showNotificationStatus Pass false if you need to handle downloading process callback ui by yourself, like showing [ProgressDialog] or
     *  custom [Notification] when download is running
     * @param downloadStatusCallback Optional call back with the downloading status
     * @throws RuntimeException If needed permissions not handled or granted by user
     * @throws RuntimeException If storage type in not external
     * @throws RuntimeException If APi method is post
     * @return Unique id of current downloading process [downloadId]**/
    fun download(
        downloadUrl: String,
        httpType: HttpType = HttpType.GET,
        storageType: StorageType,
        syncWithGallery: Boolean? = null,
        folderName: String? = null,
        fileName: String?,
        mimeType: String?,
        headers: Map<String, Any>? = null,
        showNotificationStatus: Boolean? = null,
        body: Any? = null,
        downloadStatusCallback: DownloadStatusCallback? = null
    ): Long?


    /**
     *  Implemented by each Download component to handle cancel downloading action
     * @param downloadID Id of download process needed to be canceled
     * @return Number of deleted rows*/
    fun cancel(downloadID: Long): Int
}