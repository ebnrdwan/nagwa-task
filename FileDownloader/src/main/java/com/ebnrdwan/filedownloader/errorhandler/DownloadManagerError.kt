


package com.ebnrdwan.filedownloader.errorhandler

import android.app.DownloadManager

/*Representing possible download manager errors*/
sealed class DownloadManagerError(val reasonCode: Int): IDownloadError {

    companion object {
        const val NOT_FOUND = 404
        const val SERVER_ERROR = 500
        /**Get Download manager error instance by reason of error
         * @param reason Reason of error
         * @return [DownloadManagerError]*/
        fun getErrorTypeBy(reason: Int): DownloadManagerError {
            return when (reason) {
                NOT_FOUND -> return ERROR_NOT_FOUND()
                SERVER_ERROR -> return ERROR_SERVER()
                DownloadManager.ERROR_FILE_ERROR -> return ERROR_FILE_ERROR()
                DownloadManager.ERROR_UNHANDLED_HTTP_CODE -> return ERROR_UNHANDLED_HTTP_CODE()
                DownloadManager.ERROR_HTTP_DATA_ERROR -> return ERROR_HTTP_DATA_ERROR()
                DownloadManager.ERROR_TOO_MANY_REDIRECTS -> return ERROR_TOO_MANY_REDIRECTS()
                DownloadManager.ERROR_INSUFFICIENT_SPACE -> return ERROR_INSUFFICIENT_SPACE()
                DownloadManager.ERROR_CANNOT_RESUME -> return ERROR_CANNOT_RESUME()
                DownloadManager.ERROR_FILE_ALREADY_EXISTS -> return ERROR_FILE_ALREADY_EXISTS()
                DownloadManager.ERROR_DEVICE_NOT_FOUND -> return ERROR_UNKNOWN()
                else -> ERROR_UNKNOWN()
            }
        }
    }

    /**
     * Value of COLUMN_ERROR_CODE when the download has completed with an error that doesn't fit
     * under any other error code.
     */
    class ERROR_UNKNOWN(reason: Int = DownloadManager.ERROR_UNKNOWN) : DownloadManagerError(reason)

    /**
     * Value of {@link #COLUMN_REASON} when a storage issue arises which doesn't fit under any
     * other error code. Use the more specific {@link #ERROR_INSUFFICIENT_SPACE} and
     * {@link #ERROR_DEVICE_NOT_FOUND} when appropriate.
     */
    class ERROR_FILE_ERROR(reason: Int = DownloadManager.ERROR_FILE_ERROR) :
        DownloadManagerError(reason)

    /**
     * Value of {@link #COLUMN_REASON} when an HTTP code was received that download manager
     * can't handle.
     */
    class ERROR_UNHANDLED_HTTP_CODE(reason: Int = DownloadManager.ERROR_UNHANDLED_HTTP_CODE) :
        DownloadManagerError(reason)

    /**
     * Value of {@link #COLUMN_REASON} when an error receiving or processing data occurred at
     * the HTTP level.
     */
    class ERROR_HTTP_DATA_ERROR(reason: Int = DownloadManager.ERROR_HTTP_DATA_ERROR) :
        DownloadManagerError(reason)

    /**
     * Value of {@link #COLUMN_REASON} when there were too many redirects.
     */
    class ERROR_TOO_MANY_REDIRECTS(reason: Int = DownloadManager.ERROR_TOO_MANY_REDIRECTS) :
        DownloadManagerError(reason)


    /**
     * Value of {@link #COLUMN_REASON} when there was insufficient storage space. Typically,
     * this is because the SD card is full.
     */
    class ERROR_INSUFFICIENT_SPACE(reason: Int = DownloadManager.ERROR_INSUFFICIENT_SPACE) :
        DownloadManagerError(reason)

    /**
     * Value of {@link #COLUMN_REASON} when some possibly transient error occurred but we can't
     * resume the download.
     */
    class ERROR_CANNOT_RESUME(reason: Int = DownloadManager.ERROR_CANNOT_RESUME) :
        DownloadManagerError(reason)

    /**
     * Value of {@link #COLUMN_REASON} when the requested destination file already exists (the
     * download manager will not overwrite an existing file).
     */
    class ERROR_FILE_ALREADY_EXISTS(reason: Int = DownloadManager.ERROR_FILE_ALREADY_EXISTS) :
        DownloadManagerError(reason)


    /**
     *When the download api respond with 404
     */
    class ERROR_NOT_FOUND(reason: Int = NOT_FOUND) : DownloadManagerError(reason)

    /**
     *When the download api respond with 500
     */
    class ERROR_SERVER(reason: Int = SERVER_ERROR) : DownloadManagerError(reason)
}