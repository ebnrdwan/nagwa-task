


package com.ebnrdwan.filedownloader.errorhandler

import android.app.DownloadManager

/*Representing possible download manager errors*/
sealed class WorkManagerError(val reasonCode: Int): IDownloadError {

    companion object {
        const val NOT_FOUND = 404
        const val SERVER_ERROR = 500
        /**Get Download manager error instance by reason of error
         * @param reason Reason of error
         * @return [WorkManagerError]*/
        fun getErrorTypeBy(reason: Int): WorkManagerError {
            return when (reason) {
                NOT_FOUND -> return ERROR_NOT_FOUND()
                SERVER_ERROR -> return ERROR_SERVER()
                else -> ERROR_UNKNOWN()
            }
        }
    }

    /**
     * Value of COLUMN_ERROR_CODE when the download has completed with an error that doesn't fit
     * under any other error code.
     */
    class ERROR_UNKNOWN(reason: Int = DownloadManager.ERROR_UNKNOWN) : WorkManagerError(reason)


    /**
     *When the download api respond with 404
     */
    class ERROR_NOT_FOUND(reason: Int = NOT_FOUND) : WorkManagerError(reason)

    /**
     *When the download api respond with 500
     */
    class ERROR_SERVER(reason: Int = SERVER_ERROR) : WorkManagerError(reason)
}