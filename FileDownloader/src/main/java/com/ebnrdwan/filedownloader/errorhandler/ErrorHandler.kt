package com.ebnrdwan.filedownloader.errorhandler

import com.ebnrdwan.filedownloader.models.DownloadType

/**this class is Responsible for mapping error reason or error code to error model based on Download Type
 *error models is responsible to provide the corresponding info for the error */
object ErrorHandler {

    /**return error model of such error reason or code
     * @param reason int code is returned from a download types which are
     * [com.ebnrdwan.filedownloader.downloadmanager.DownloadManager]
     * [com.ebnrdwan.filedownloader.workmanager.WorkManager]
     *
     * @param downloadType representing the download type that request this error mapping
     * based on type method is returning the corresponding error model*/
    fun getErrorTypeBy(reason: Int, downloadType: DownloadType): IDownloadError {
        return when (downloadType) {
            DownloadType.DOWNLOAD_MANAGER -> DownloadManagerError.getErrorTypeBy(reason)
            DownloadType.WORK_MANAGER -> WorkManagerError.getErrorTypeBy(reason)
        }
    }
}