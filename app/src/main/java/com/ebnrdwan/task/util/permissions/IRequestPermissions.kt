package com.ebnrdwan.task.util.permissions

/**
 * interface to handle different permissions state
 */
interface IRequestPermissions {

    /**
     * Called after the permission granted
     */
    fun onPermissionGranted(permission: String)

    /**
     * Called right after the system permissions dialogs if the user checks "never ask again"
     *     on any of the requested permissions.
     */
    fun onNeverAskAgainChecked(permission: String)

    /**
     * Called when user denied the permission
     */

    fun onPermissionDenied(permission: String)


}