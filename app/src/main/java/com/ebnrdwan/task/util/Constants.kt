package com.ebnrdwan.task.util

import android.Manifest

object Constants {

    object Ui {
        const val DEFAULT_RATE = 1.0
        const val DEFAULT_DECIMALS = 2
        const val DEFAULT_SELECTION_COUNT = 1
    }

    object Files {
        const val FOLDER_NAME = "NAGWA_APP"
        const val filesLocalResponse = "api_responses/ListOfFilesResponse.json"
        const val FILE_TYPE_VIDEO="VIDEO"
        const val FILE_TYPE_PDF="PDF"
    }

    object Logging {
        const val RX_TAG_ERROR = "RX_ERROR"
        const val RX_TAG_SUCCESS = "RX_SUCCESS"
        const val RX_TAG_NEXT = "RX_NEXT"
        const val RX_TAG_COMPLETE = "RX_COMPLETE"
        const val RX_TAG_SUPSCRIBE = "RX_COMPLETE"
    }

    object RequestCodes {
        const val REQUEST_APP_SETTINGS = 1000
    }

    object PermissionTags {
        const val WRITE_EXTERNAL_STORAGE_TAG = "WRITE_EXTERNAL_STORAGE_TAG"
        const val READ_EXTERNAL_STORAGE_TAG = Manifest.permission.READ_EXTERNAL_STORAGE
        const val FILE_PERMISSIONS_TAG = "FILE_PERMISSIONS_TAG"
    }


}
