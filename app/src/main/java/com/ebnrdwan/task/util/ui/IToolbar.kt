package com.ebnrdwan.task.util.ui

import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.TypefaceSpan
import android.view.MenuItem
import androidx.annotation.FontRes
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.ebnrdwan.task.R


interface IToolbar {
    var mToolbar: Toolbar?


    fun setToolbarTitle(title: String): Toolbar? {
        mToolbar?.title = title

        return mToolbar
    }

    fun setToolbarBackGround(color: Int): Toolbar? {
        mToolbar?.setBackgroundColor(color);
        return mToolbar
    }

    fun setToolbarSubTitle(subtitle: String): Toolbar? {
        mToolbar?.subtitle = subtitle
        return mToolbar
    }

    fun changeMoreMenuItemsTextColor(mContext: Context, menuItemList: List<MenuItem?>) {
        menuItemList.forEach { item ->
            var spannableStringBuilder = SpannableStringBuilder(item?.title)
            spannableStringBuilder = setMenuItemTextColor(spannableStringBuilder, mContext)

            item?.title = spannableStringBuilder
        }
    }

    fun setMenuItemTextColor(
        spannableStringBuilder: SpannableStringBuilder,
        mContext: Context
    ): SpannableStringBuilder {
        spannableStringBuilder.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.black)),
            0,
            spannableStringBuilder.length,
            0
        )
        return spannableStringBuilder
    }

    fun setMenuItemFont(
        mContext: Context,
        spannable: SpannableStringBuilder,
        item: MenuItem?,
        @FontRes font: Int
    ): SpannableStringBuilder {
        val typeFace = mContext.applicationContext?.let { ResourcesCompat.getFont(it, font) }
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            val typefaceSpan = typeFace?.let { TypefaceSpan(it) }
            spannable.setSpan(
                typefaceSpan,
                0,
                item?.title?.length ?: 0,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
            )
            spannable
        } else {
            val customTypefaceSpan: CustomTypefaceSpan =
                if (typeFace != null) CustomTypefaceSpan(typeFace) else CustomTypefaceSpan(
                    Typeface.defaultFromStyle(
                        Typeface.NORMAL
                    )
                )
            spannable.setSpan(
                customTypefaceSpan,
                0,
                item?.title?.length ?: 0,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
            )
            spannable
        }
    }
}