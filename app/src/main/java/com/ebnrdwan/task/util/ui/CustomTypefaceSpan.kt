package com.ebnrdwan.task.util.ui

import android.graphics.Paint
import android.graphics.Typeface

import android.text.TextPaint

import android.text.style.MetricAffectingSpan


class CustomTypefaceSpan(private val typeface: Typeface) : MetricAffectingSpan() {
    override fun updateDrawState(drawState: TextPaint) {
        apply(drawState)
    }

    override fun updateMeasureState(paint: TextPaint) {
        apply(paint)
    }

    private fun apply(paint: Paint) {
        val oldTypeface: Typeface = paint.typeface
        val oldStyle = oldTypeface.style
        val style = oldStyle and typeface.style.inv()
        if (style and Typeface.BOLD != 0) {
            paint.isFakeBoldText = true
        }
        paint.typeface = typeface
    }

}