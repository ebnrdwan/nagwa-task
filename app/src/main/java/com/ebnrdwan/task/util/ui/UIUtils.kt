package com.ebnrdwan.task.util.ui

import android.content.Context
import android.content.DialogInterface
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

/**
 * utils class for ui helper methods
 */
object UIUtils {
    /**
     * Call this method to show dialog
     * [context] current context
     * [title] title of the dialog
     * [message] message of the dialog
     * [positiveButton] positive button string
     * [negativeButton] negative button string
     * [cancelable] boolean indicate if the dialog is cancelable or not
     * [positiveClickListener] the click listener of the positive button
     * [negativeClickListener]the click listener of the negative button
     */
    fun showBasicDialog(
        context: Context, title: String? = null, message: String? = null,
        positiveButton: String, negativeButton: String? = null,
        cancelable: Boolean = true,
        positiveClickListener: DialogInterface.OnClickListener,
        negativeClickListener: DialogInterface.OnClickListener? = null
    ): AlertDialog {
        return AlertDialog.Builder(context)
            .setTitle(title)
            .setCancelable(cancelable)
            .setMessage(message)
            .setPositiveButton(positiveButton, positiveClickListener)
            .setNegativeButton(negativeButton, negativeClickListener)
            .show()
    }

    /**
     * Calls to display a toast message
     * [context] current context
     * [message] message that needed to display
     */
    fun showToast(context: Context?, message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}