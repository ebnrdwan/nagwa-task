package com.ebnrdwan.task.util

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.OpenableColumns
import android.webkit.MimeTypeMap
import androidx.core.content.FileProvider
import androidx.exifinterface.media.ExifInterface
import com.ebnrdwan.filedownloader.models.MimeType
import com.ebnrdwan.task.R
import com.ebnrdwan.task.util.ui.UIUtils
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*


object AppFileUtils {
    const val TAG = "FilePickerTag"

    // get file extension
    fun getExtensionFromUri(context: Context?, uri: Uri?): String? {
        var mimeType: String? = ""

        mimeType = if (uri?.scheme == ContentResolver.SCHEME_CONTENT) {
            //If scheme is a content
            val mime = MimeTypeMap.getSingleton()
            mime.getExtensionFromMimeType(context?.contentResolver?.getType(uri))
        } else
            MimeTypeMap.getFileExtensionFromUrl(uri.toString())

        return mimeType
    }

    // get file name
    fun getFileNameFromUri(context: Context, uri: Uri): String {
        return try {
            val returnCursor = context.contentResolver.query(uri, null, null, null, null)!!
            val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            returnCursor.moveToFirst()
            val name = returnCursor.getString(nameIndex)
            returnCursor.close()
            name.substring(0, name.lastIndexOf('.'))
        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }
    }

    fun getFileFromPath(filePath: String?): File? {
        return if (filePath != null) File(filePath) else null
    }

    @Throws(Exception::class)
    fun copyStream(inputStream: InputStream, outputStream: FileOutputStream) {
        val BUFFER_SIZE = 4096
        val bytes = ByteArray(BUFFER_SIZE)
        var count = 0
        while (count != -1) {
            count = inputStream.read(bytes)
            if (count != -1) {
                outputStream.write(bytes, 0, count)
            }
        }
        outputStream.flush()
        inputStream.close()
        outputStream.close()
    }



    private fun bytesToMegaBytes(bytes: Long): Double {
        return bytes.toDouble() / (1000 * 1000)
    }



    fun getMimeTypes(): Set<MimeType> {
        val mimeTypeList: ArrayList<MimeType> = ArrayList()
        mimeTypeList.add(MimeType.ALL_IMAGES)
        mimeTypeList.add(MimeType.ALL_VIDEOS)
        mimeTypeList.add(MimeType.ALL_FILES)
        mimeTypeList.add(MimeType.ALL_AUDIO)
        mimeTypeList.add(MimeType.PDF)
        mimeTypeList.add(MimeType.TXT)
        mimeTypeList.add(MimeType.DOC)
        mimeTypeList.add(MimeType.DOCX)
        mimeTypeList.add(MimeType.PTT)
        mimeTypeList.add(MimeType.PTTX)
        mimeTypeList.add(MimeType.XLS)

        return mimeTypeList.toSet()
    }

    internal fun getFileUri(context: Context, fileUrl: String): Uri {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            FileProvider.getUriForFile(context, context.packageName + ".provider", File(fileUrl))
        } else {
            Uri.fromFile(File(fileUrl))
        }
    }






    //    fun getFileName(filePath: String): String = File(filePath).name
    fun getFileName(filePath: String) =
        filePath.substring(filePath.lastIndexOf("/") + 1)

    fun getAppDirectory(context: Context): String {
        val folder = File(
            context.getExternalFilesDir(null)?.absolutePath
                    + "/"
                    + Constants.Files.FOLDER_NAME
        )
        folder.mkdirs()
        return folder.path
    }



    fun checkFileExistsInAppDirectory(context: Context, fileName: String): Boolean {
        val fileUrl = "${getAppDirectory(context)}/${fileName}"
        return File(fileUrl).exists()
    }


    fun openFile(context: Context, fileURI: String, fileType: String) {
        val intent = Intent(Intent.ACTION_VIEW)

        val data = getFileUri(
            context,
            fileURI
        )

        intent.setDataAndType(data, fileType)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        if (intent.resolveActivity(context.packageManager) != null) {
            context.startActivity(intent)
        } else {
            UIUtils.showToast(
                context,
                context.getString(R.string.openFileFail)
            )
        }
    }

     fun isInvalidUrl(fileUrl: String?): Boolean {
        if (fileUrl.isNullOrEmpty()) {
            return true
        }
        return false
    }

     fun isInvalidExtension(fileUrl: String?): Boolean {
        if (checkFileExtension(fileUrl)) {
            return true
        }
        return false
    }

     fun isFileExistBefore(context:Context, fileUrl: String?): Boolean {
        if (fileUrl.isNullOrEmpty()) return false
        val fileName = getFileName(fileUrl)
        if (checkFileExistsInAppDirectory(context, fileName)) {
            return true
        }
        return false
    }

     fun checkFileExtension(fileUrl: String?): Boolean {
        return getMimeTypes()
            .map { it.fileExtension }.firstOrNull {
                it.contains(getExtensionFromURL(fileUrl ?: "")?.toLowerCase(Locale.ROOT))
            }.isNullOrEmpty()
    }

    fun getExtensionFromURL(url: String): String? {
        return try {
            return url.substring(url.lastIndexOf(".") + 1)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }

    }

    fun getFileUriFromFileName(context: Context, fileName: String): String {
        return "${getAppDirectory(context)}/${fileName}"

    }
    fun getMimeTypeFromURL(url: String): String? {
        return try {
            val ext = url.substring(url.lastIndexOf(".") + 1)
            val mime = MimeTypeMap.getSingleton()
            mime.getMimeTypeFromExtension(ext)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }

    }
}