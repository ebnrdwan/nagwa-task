package com.ebnrdwan.task.util

import android.content.Context
import java.io.IOException
import java.nio.charset.Charset

fun loadJsonFromTestResources(context: Context, filePath: String): String? {
    var json: String? = null
    try {
        val stream =context.assets.open(filePath)

        val size = stream.available()

        val buffer = ByteArray(size)

        stream.read(buffer)

        stream.close()

        json = String(buffer, Charset.defaultCharset())


    } catch (ex: IOException) {
        ex.printStackTrace()
        return null
    }

    return json

}