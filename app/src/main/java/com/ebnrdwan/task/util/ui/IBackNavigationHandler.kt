package com.ebnrdwan.task.util.ui

import androidx.activity.addCallback
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner

interface IBackNavigationHandler {


    fun registerHardwareBackWithNavigationComponent(
        activity: FragmentActivity,
        lifecycleOwner: LifecycleOwner,
        onNavigateUp: () -> Unit
    ) {
        activity.onBackPressedDispatcher.addCallback(lifecycleOwner) {
            onNavigateUp()
        }
    }

}