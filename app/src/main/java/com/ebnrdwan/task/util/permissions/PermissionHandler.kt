package com.ebnrdwan.task.util.permissions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.app.ActivityCompat.shouldShowRequestPermissionRationale
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.ebnrdwan.task.util.Constants
import com.ebnrdwan.task.util.ui.UIUtils

/**
 * handle all permissions
 * [fragment] the host fragment
 * [activity] the host activity
 * [iRequestPermissions reference to [IRequestPermissions]
 */
 class PermissionHandler(
    private val fragment: Fragment?,
    private val activity: Activity?,
    private val iRequestPermissions: IRequestPermissions
) {

    private var mPermissionsTag: String? = null
    lateinit var mContext: Context

    companion object {

        private const val MY_PERMISSIONS_REQUEST = 1
    }

    private var notGrantedPermissions = ArrayList<String>()


    fun checkPermissions(
        context: Context, tag: String? = null, message: String = "",
        permissions: ArrayList<String>,
        showRationalPositiveActionText: String? = null,
        showRationalNegativeActionText: String? = null,

        ) {
        mContext = context
        if (permissions.isEmpty())
            throw Exception("Check permission called without any permissions")
        mPermissionsTag = tag
        notGrantedPermissions = ArrayList() // reset

        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(
                    context,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                notGrantedPermissions.add(permission)
            }
        }

        if (notGrantedPermissions.isEmpty()) {
            iRequestPermissions.onPermissionGranted(tag ?: permissions[0])
        } else {
            if (shouldShowRationale(notGrantedPermissions)) {
                showCustomDialog(
                    message,
                    showRationalPositiveActionText,
                    showRationalNegativeActionText
                )
            } else {
                if (fragment != null) {
                    fragment.requestPermissions(
                        notGrantedPermissions.toTypedArray(),
                        MY_PERMISSIONS_REQUEST
                    )
                } else if (activity != null) {
                    requestPermissions(
                        activity,
                        notGrantedPermissions.toTypedArray(),
                        MY_PERMISSIONS_REQUEST
                    )
                }
            }
        }
    }

    fun onHandleActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.RequestCodes.REQUEST_APP_SETTINGS) {
            var allPermissionsGranted = true

            for (permission in notGrantedPermissions) {
                if (!isPermissionGranted(permission)) {
                    allPermissionsGranted = false
                }
            }

            when {
                allPermissionsGranted -> iRequestPermissions.onPermissionGranted(
                    mPermissionsTag
                        ?: notGrantedPermissions[0]
                )
                else -> iRequestPermissions.onPermissionDenied(
                    mPermissionsTag
                        ?: notGrantedPermissions[0]
                )
            }
        }
    }

    /**
     * Handle permissions requested by looping on the permissions and
     * setting two flags one for when all permissions are granted and one for when never ask again checked in any of the permissions.
     */
    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>, grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST -> {
                if (grantResults.isNotEmpty()) {
                    var allPermissionsGranted = true
                    var shouldShowRationale = true

                    for (grantResult in grantResults) {
                        if (grantResult == PackageManager.PERMISSION_DENIED) {
                            allPermissionsGranted = false
                            val showRationale = fragment?.shouldShowRequestPermissionRationale(
                                permissions[grantResults.indexOf(grantResult)]
                            )
                                ?: if (activity != null) {
                                    shouldShowRequestPermissionRationale(
                                        activity,
                                        permissions[grantResults.indexOf(grantResult)]
                                    )
                                } else {
                                    false
                                }
                            if (!showRationale) {
                                shouldShowRationale = false
                                break
                            }
                        }
                    }

                    when {
                        !shouldShowRationale -> iRequestPermissions.onNeverAskAgainChecked(
                            mPermissionsTag
                                ?: permissions[0]
                        )
                        allPermissionsGranted -> iRequestPermissions.onPermissionGranted(
                            mPermissionsTag
                                ?: permissions[0]
                        )
                        else -> iRequestPermissions.onPermissionDenied(
                            mPermissionsTag
                                ?: permissions[0]
                        )
                    }
                }
            }
        }
    }

    private fun isPermissionGranted(permission: String): Boolean {
        return ActivityCompat.checkSelfPermission(
            mContext,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun shouldShowRationale(permissions: ArrayList<String>): Boolean {
        var isNeed = false
        for (i in permissions.indices) {
            if (fragment != null) {
                if (fragment.shouldShowRequestPermissionRationale(permissions[i])) {
                    isNeed = true
                    break
                }
            } else if (activity != null) {
                if (shouldShowRequestPermissionRationale(activity, permissions[i])) {
                    isNeed = true
                    break
                }
            }

        }
        return isNeed
    }


    private fun showCustomDialog(
        message: String, showRationalPositiveActionText: String? = null,
        showRationalNegativeActionText: String? = null
    ) {

        UIUtils.showBasicDialog(mContext, null, message,
            showRationalPositiveActionText ?: "", showRationalNegativeActionText, false,
            { _, _ ->
                if (fragment != null) {
                    fragment.requestPermissions(
                        notGrantedPermissions.toTypedArray(),
                        MY_PERMISSIONS_REQUEST
                    )
                } else if (activity != null) {
                    requestPermissions(
                        activity,
                        notGrantedPermissions.toTypedArray(),
                        MY_PERMISSIONS_REQUEST
                    )
                }
            },
            { _, _ ->
                iRequestPermissions.onPermissionDenied(mPermissionsTag ?: notGrantedPermissions[0])
            })

    }


}