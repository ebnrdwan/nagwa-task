package com.ebnrdwan.task.data.sources.files


import com.ebnrdwan.core.data.sources.IDataSource
import com.ebnrdwan.task.data.dto.files.FilesResponse
import io.reactivex.Single

interface IFilesDataSource : IDataSource {
    fun getFiles(): Single<FilesResponse>
}