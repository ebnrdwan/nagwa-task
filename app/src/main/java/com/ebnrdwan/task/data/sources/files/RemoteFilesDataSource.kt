package com.ebnrdwan.task.data.sources.files

import com.ebnrdwan.task.BuildConfig
import com.ebnrdwan.task.data.dto.files.FilesResponse
import com.ebnrdwan.task.data.service.FilesApi
import io.reactivex.Single

open class RemoteFilesDataSource constructor(private val service: FilesApi) : IFilesDataSource {
    override fun getFiles(): Single<FilesResponse> {
        return service.getCurrencies(BuildConfig.API_KEY)
    }
}