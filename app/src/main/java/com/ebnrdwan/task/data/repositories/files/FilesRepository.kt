package com.ebnrdwan.task.data.repositories.files

import android.content.Context
import com.ebnrdwan.core.data.repositories.BaseRepository
import com.ebnrdwan.core.di.RemoteFileSource
import com.ebnrdwan.corepresentation.utils.NoInternetException
import com.ebnrdwan.filedownloader.factory.IDownloadFactory
import com.ebnrdwan.filedownloader.interactions.DownloadStatusCallback
import com.ebnrdwan.filedownloader.models.HttpType
import com.ebnrdwan.filedownloader.models.StorageType
import com.ebnrdwan.task.R
import com.ebnrdwan.task.data.dto.files.FilesResponse
import com.ebnrdwan.task.data.sources.files.IFilesDataSource
import com.ebnrdwan.task.util.Constants
import io.reactivex.Single
import javax.inject.Inject


class FilesRepository @Inject constructor(
    @RemoteFileSource private val filesDataSource: IFilesDataSource,
    private val mContext: Context
) : BaseRepository(mContext), IFilesRepository {
    override fun fetchFiles():
            Single<FilesResponse> {

        return checkInternetConnection()
            .flatMap { isConnected ->
                if (isConnected) filesDataSource.getFiles()
                else Single.error(
                    NoInternetException(
                        mContext.resources.getString(
                            R.string.no_internet
                        )
                    )
                )
            }
    }


    override fun downloadFile(
        downloadFactory: IDownloadFactory,
        context: Context,
        downloadUrl: String,
        showNotification: Boolean?,
        downloadStatusCallback: DownloadStatusCallback?
    ): Single<Long> {
        return Single.just(
            downloadFactory.download(
                downloadUrl = downloadUrl,
                httpType = HttpType.GET,
                storageType = StorageType.ExternalAppSpecificFiles,
                syncWithGallery = false,
                folderName = Constants.Files.FOLDER_NAME,
                showNotificationStatus = showNotification,
                downloadStatusCallback = downloadStatusCallback,
                fileName = null,
                mimeType = null
            ) ?: 0L
        )
    }
}

