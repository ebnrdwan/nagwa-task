package com.ebnrdwan.task.data.dto.files

import com.google.gson.annotations.SerializedName

data class FileItem(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("type")
    val type: String = "",
    @SerializedName("url")
    val url: String = "",
    var selected: Boolean = false,
    var downloaded: Boolean = false
)