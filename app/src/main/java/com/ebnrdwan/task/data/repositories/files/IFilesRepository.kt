package com.ebnrdwan.task.data.repositories.files

import android.content.Context
import com.ebnrdwan.filedownloader.factory.IDownloadFactory
import com.ebnrdwan.filedownloader.interactions.DownloadStatusCallback
import com.ebnrdwan.task.data.dto.files.FilesResponse
import io.reactivex.Single


interface IFilesRepository {
    fun fetchFiles(): Single<FilesResponse>
    fun downloadFile(downloadFactory: IDownloadFactory,
                     context: Context,
                     downloadUrl: String,
                     showNotification: Boolean? = null,
                     downloadStatusCallback: DownloadStatusCallback? = null): Single<Long>
}

