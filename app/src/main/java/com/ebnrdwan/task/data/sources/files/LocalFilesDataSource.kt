package com.ebnrdwan.task.data.sources.files

import android.content.Context
import com.ebnrdwan.task.data.dto.files.FilesResponse
import com.ebnrdwan.task.util.Constants.Files.filesLocalResponse
import com.ebnrdwan.task.util.loadJsonFromTestResources
import com.google.gson.Gson
import io.reactivex.Single

open class LocalFilesDataSource(val context: Context) : IFilesDataSource {
    override fun getFiles(): Single<FilesResponse> {
        val data = loadJsonFromTestResources(context, filesLocalResponse)
        val filesResponse = Gson().fromJson(data, FilesResponse::class.java)
        return Single.just(filesResponse)
    }
}

