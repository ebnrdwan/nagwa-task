package com.ebnrdwan.task.di.files

import com.ebnrdwan.core.di.FragmentScope
import com.ebnrdwan.task.presentation.files.FileListFragment
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = [FilesModule::class])
interface FilesSubcomponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): FilesSubcomponent
    }

    fun inject(fileListFragment: FileListFragment)
}