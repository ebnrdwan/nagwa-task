package com.ebnrdwan.task.di.files

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import com.ebnrdwan.core.di.FragmentScope
import com.ebnrdwan.core.di.LocalFileSource
import com.ebnrdwan.core.di.RemoteFileSource
import com.ebnrdwan.core.di.ViewModelKey
import com.ebnrdwan.task.data.repositories.files.FilesRepository
import com.ebnrdwan.task.data.repositories.files.IFilesRepository
import com.ebnrdwan.task.data.service.FilesApi
import com.ebnrdwan.task.data.sources.files.IFilesDataSource
import com.ebnrdwan.task.data.sources.files.LocalFilesDataSource
import com.ebnrdwan.task.data.sources.files.RemoteFilesDataSource
import com.ebnrdwan.task.domain.downloadfiles.DownloadFilesUseCase
import com.ebnrdwan.task.domain.downloadfiles.IDownloadFilesUseCase
import com.ebnrdwan.task.domain.fetchfiles.FetchFilesUseCase
import com.ebnrdwan.task.domain.fetchfiles.IFilesUseCase
import com.ebnrdwan.task.presentation.files.FilesViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module(includes = [BindViewModel::class])
object FilesModule {

    @FragmentScope
    @JvmStatic
    @Provides
    fun bindApiFileServices(retrofit: Retrofit): FilesApi =
        retrofit.create(FilesApi::class.java)


    @FragmentScope
    @JvmStatic
    @RemoteFileSource
    @Provides
    fun bindRemoteFileDataSource(apiService: FilesApi): IFilesDataSource =
        RemoteFilesDataSource(apiService)


    @FragmentScope
    @JvmStatic
    @LocalFileSource
    @Provides
    fun bindLocalFileDataSource(context: Context): IFilesDataSource = LocalFilesDataSource(context)


    @FragmentScope
    @JvmStatic
    @Provides
    fun provideFilesRepository(
        application: Application,
        @LocalFileSource filesDataSource: IFilesDataSource
    ): IFilesRepository = FilesRepository(filesDataSource, application)

    @FragmentScope
    @JvmStatic
    @Provides
    fun provideFilesUseCase(
        repository: IFilesRepository
    ): IFilesUseCase =
        FetchFilesUseCase(repository)

    @FragmentScope
    @JvmStatic
    @Provides
    fun provideDownloadFilesUseCase(
        repository: IFilesRepository
    ): IDownloadFilesUseCase =
        DownloadFilesUseCase(repository)

}

@Module
abstract class BindViewModel {
    @FragmentScope
    @Binds
    @IntoMap
    @ViewModelKey(FilesViewModel::class)
    abstract fun bindViewModel(viewModel: FilesViewModel): ViewModel

}