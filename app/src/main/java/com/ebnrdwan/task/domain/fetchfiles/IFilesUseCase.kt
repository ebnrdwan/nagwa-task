package com.ebnrdwan.task.domain.fetchfiles

import com.ebnrdwan.task.data.dto.files.FilesResponse
import io.reactivex.Single

interface IFilesUseCase {

   operator fun invoke(): Single<FilesResponse>
}