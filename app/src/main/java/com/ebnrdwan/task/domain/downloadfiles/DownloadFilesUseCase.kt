package com.ebnrdwan.task.domain.downloadfiles

import android.content.Context
import com.ebnrdwan.filedownloader.factory.IDownloadFactory
import com.ebnrdwan.filedownloader.interactions.DownloadStatusCallback
import com.ebnrdwan.task.data.repositories.files.IFilesRepository
import io.reactivex.Single
import javax.inject.Inject

class DownloadFilesUseCase @Inject constructor(private val repository: IFilesRepository) :
    IDownloadFilesUseCase {
    /* invoke is the only exposed function in use-case
        to insure that use-cases has only one and single responsibility*/
    override operator fun invoke(
        downloadFactory: IDownloadFactory,
        context: Context,
        downloadUrl: String,
        showNotification: Boolean?,
        downloadStatusCallback: DownloadStatusCallback?
    ): Single<Long> = repository.downloadFile(
        downloadFactory,
        context,
        downloadUrl,
        showNotification,
        downloadStatusCallback
    )
}