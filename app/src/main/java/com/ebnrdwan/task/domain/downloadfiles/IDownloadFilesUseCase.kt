package com.ebnrdwan.task.domain.downloadfiles

import android.content.Context
import com.ebnrdwan.filedownloader.factory.IDownloadFactory
import com.ebnrdwan.filedownloader.interactions.DownloadStatusCallback
import com.ebnrdwan.task.data.dto.files.FilesResponse
import io.reactivex.Single

interface IDownloadFilesUseCase {

   operator fun invoke(downloadFactory: IDownloadFactory,
                       context: Context,
                       downloadUrl: String,
                       showNotification: Boolean? = null,
                       downloadStatusCallback: DownloadStatusCallback? = null): Single<Long>
}