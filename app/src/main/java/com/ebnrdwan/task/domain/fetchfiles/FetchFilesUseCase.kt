package com.ebnrdwan.task.domain.fetchfiles

import com.ebnrdwan.task.data.dto.files.FilesResponse
import com.ebnrdwan.task.data.repositories.files.IFilesRepository
import com.ebnrdwan.task.domain.fetchfiles.IFilesUseCase
import io.reactivex.Single
import javax.inject.Inject

class FetchFilesUseCase @Inject constructor(private val repository: IFilesRepository) :
    IFilesUseCase {
    /* invoke is the only exposed function in use-case
        to insure that use-cases has only one and single responsibility*/
    override operator fun invoke(): Single<FilesResponse> = repository.fetchFiles()
}