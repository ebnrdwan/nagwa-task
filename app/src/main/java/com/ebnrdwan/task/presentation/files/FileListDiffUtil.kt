package com.ebnrdwan.task.presentation.files

import androidx.recyclerview.widget.DiffUtil
import com.ebnrdwan.task.data.dto.files.FileItem

class FileListDiffUtil : DiffUtil.ItemCallback<FileItem>() {

    override fun areItemsTheSame(oldItem: FileItem, fileItem: FileItem): Boolean =
        oldItem.name == fileItem.name

    override fun areContentsTheSame(oldItem: FileItem, fileItem: FileItem): Boolean =
        oldItem == fileItem
}