package com.ebnrdwan.task.presentation.files

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ebnrdwan.task.R
import com.ebnrdwan.task.data.dto.files.FileItem
import com.ebnrdwan.task.util.Constants
import getColorEx
import kotlinx.android.synthetic.main.file_list_item.view.*
import setVisibilityState


class FilesAdapterPaged
constructor(
    val context: Context,
    private var list: List<FileItem>,
    val onClickCallback: (FileItem) -> Unit
) :
    PagedListAdapter<FileItem, FilesAdapterPaged.ViewHolder>(
        FileListDiffUtil()
    ) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rootView: View =
            LayoutInflater.from(context)
                .inflate(R.layout.file_list_item, parent, false)
        return ViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val fileItem = list[position]
        fileItem.let { holder.bind(fileItem) }
    }


    fun updateFiles(listCurrencies: List<FileItem>) {
        list = listCurrencies
        notifyDataSetChanged()
    }

    // total number of rows
    override fun getItemCount(): Int {
        return list.size
    }


    // stores and recycles views as they are scrolled off screen
    inner class ViewHolder internal constructor(private val rootView: View) :
        RecyclerView.ViewHolder(rootView) {
        init {
            rootView.setOnClickListener { onClickCallback.invoke(list[adapterPosition]) }
        }

        fun bind(fileItem: FileItem) {
            rootView.tv_title.text = fileItem.name
            bindItemBackgroundColor(fileItem)
            bindItemIcon(fileItem)
            bindIconStatus(fileItem)
        }

        private fun bindItemBackgroundColor(fileItem: FileItem) {
            if (fileItem.selected) rootView.item_full_view.setBackgroundColor(context.getColorEx(R.color.divider_color))
            else rootView.item_full_view.setBackgroundColor(context.getColorEx(R.color.white))
        }

        private fun bindIconStatus(fileItem: FileItem) {
            if (fileItem.downloaded) {
                rootView.ivStatus.setImageResource(R.drawable.ic_downloaded)
            }
            rootView.ivStatus.setVisibilityState(fileItem.downloaded)
        }

        private fun bindItemIcon(fileItem: FileItem) {
            when (fileItem.type) {
                Constants.Files.FILE_TYPE_PDF -> rootView.iv_file_icon.setImageResource(R.drawable.ic_pdf)
                Constants.Files.FILE_TYPE_VIDEO -> rootView.iv_file_icon.setImageResource(
                    R.drawable.ic_play_button
                )
                else -> rootView.iv_file_icon.setImageResource(R.drawable.ic_file)
            }

        }
    }

}