package com.ebnrdwan.task.presentation.main

import com.ebnrdwan.task.R
import com.ebnrdwan.corepresentation.base.BaseActivity


class MainActivity : BaseActivity() {

    override fun getLayout(): Int {
        return R.layout.activity_main
    }


    /*============ initializeScreenComponents-start================*/
    override fun initializeScreenComponents() {
    }




}
