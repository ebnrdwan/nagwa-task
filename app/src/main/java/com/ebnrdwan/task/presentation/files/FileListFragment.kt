package com.ebnrdwan.task.presentation.files

import android.Manifest
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.ebnrdwan.core.data.models.UiState
import com.ebnrdwan.corepresentation.base.BaseFragment
import com.ebnrdwan.corepresentation.base.exportErrorView
import com.ebnrdwan.corepresentation.base.exportTvError
import com.ebnrdwan.corepresentation.base.export_loading
import com.ebnrdwan.corepresentation.utils.fade
import com.ebnrdwan.corepresentation.utils.hide
import com.ebnrdwan.corepresentation.utils.show
import com.ebnrdwan.task.R
import com.ebnrdwan.task.data.dto.files.FileItem
import com.ebnrdwan.task.presentation.ApplicationController
import com.ebnrdwan.task.util.AppFileUtils
import com.ebnrdwan.task.util.Constants
import com.ebnrdwan.task.util.permissions.IRequestPermissions
import com.ebnrdwan.task.util.permissions.PermissionHandler
import com.ebnrdwan.task.util.ui.IBackNavigationHandler
import com.ebnrdwan.task.util.ui.IToolbar
import com.ebnrdwan.task.util.ui.UIUtils
import kotlinx.android.synthetic.main.fragment_files.*
import kotlinx.android.synthetic.main.selection_on_toolbar.*
import kotlinx.android.synthetic.main.toolbar.*
import setVisibilityState
import java.util.Collections.emptyList
import javax.inject.Inject


class FileListFragment : BaseFragment(), IToolbar, IRequestPermissions, IBackNavigationHandler {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val filesViewModel by activityViewModels<FilesViewModel> { viewModelFactory }
    private var filesAdapterPaged: FilesAdapterPaged? = null
    lateinit var permissionHandler: PermissionHandler
    override fun getLayout(): Int = R.layout.fragment_files

    override var mToolbar: Toolbar?
        get() = toolbar
        set(value) {}


    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.let {
            (it.application as ApplicationController)
                .appComponent.registerFilesComponent()
                .create()
                .inject(this)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        initializePermissionHandler()
        registerHardwareBackWithNavigationComponent(requireActivity(), this, ::navigateUp)

    }

    private fun initializePermissionHandler() {
        permissionHandler = PermissionHandler(this, requireActivity(), this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.select_all_action -> {
                filesViewModel.selectAllFiles()
            }
        }

        return true
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        createMenuByState(filesViewModel.selectionState, inflater, menu)
        onSelectionStateChanged(filesViewModel.selectionState)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun createMenuByState(onState: Boolean, inflater: MenuInflater, menu: Menu) {
        if (onState) {
            inflater.inflate(R.menu.files_selection_menu, menu)
            changeMoreMenuItemsTextColor(requireContext(), menu.children.toList())
            setActionBarColor(R.color.white)
        } else {
            inflater.inflate(R.menu.files_menu, menu)
        }
    }

    private fun onSelectionStateChanged(onState: Boolean) {
        selection_on_toolbar.setVisibilityState(onState)
        selection_off_toolbar.setVisibilityState(!onState)
    }


    private val refreshListener = SwipeRefreshLayout.OnRefreshListener {
        filesViewModel.reloadFiles()
    }


    override fun bindOnViewModel() {
        observeToastMessages()
        filesViewModel.loadFiles()
        observeOnFileList()
        observeOnUiState()
        observerOnSelectedFilesCounter()
        observeOnSelectionState()
    }

    override fun setListeners() {
        swipe_refresh?.setOnRefreshListener(refreshListener)
        iv_toolbar_back.setOnClickListener {
            filesViewModel.clearSelection()
        }
        iv_toolbar_download.setOnClickListener {
            permissionHandler.checkPermissions(
                requireContext(),
                Constants.PermissionTags.WRITE_EXTERNAL_STORAGE_TAG,
                getString(R.string.appWriteStorageRationalMessage),
                arrayListOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            )
        }
    }

    override fun initViews() {
        setupToolbar()
        initializeAdapter()
    }


    private fun setupToolbar() {
        (context as AppCompatActivity).setSupportActionBar(toolbar)
    }

    private fun setActionBarColor(@ColorRes color: Int) {
        (context as AppCompatActivity).supportActionBar?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context as AppCompatActivity,
                    color
                )
            )
        )
    }


    private fun observeOnFileList() {
        filesViewModel.getFilesListObservable().observe(viewLifecycleOwner, {
            filesAdapterPaged?.updateFiles(it)
        })
    }

    private fun observeToastMessages() {
        filesViewModel.getToastMessagesObservable().observeOn(viewLifecycleOwner, {
            showMessage(it)
        })
    }

    private fun observerOnSelectedFilesCounter() {
        filesViewModel.getSelectedFilesCountObservable()
            .observe(viewLifecycleOwner, { count ->
                tv_toolbar_counter.text =
                    resources.getQuantityString(R.plurals.numberOfFiles, count, count)
            })
    }

    private fun observeOnSelectionState() {
        filesViewModel.getShouldUpdateSelectionStateObservable()
            .observe(viewLifecycleOwner, { shouldUpdate ->
                if (shouldUpdate) activity?.invalidateOptionsMenu()
            })
    }


    private fun initializeAdapter() {
        filesAdapterPaged = activity?.let { activity ->
            FilesAdapterPaged(
                activity,
                emptyList()
            ) { item ->
                onFileClicked(item)
            }
        }
        rvFiles.layoutManager = LinearLayoutManager(context)
        rvFiles.adapter = filesAdapterPaged
    }

    private fun onFileClicked(fileItem: FileItem) {
        if (filesViewModel.isFileAlreadyDownloaded(fileItem)) {
            showDownloadedAlert(fileItem)
        } else filesViewModel.setSelectedFile(fileItem)
    }

    private fun showDownloadedAlert(fileItem: FileItem) {
        UIUtils.showBasicDialog(requireContext(),
            title = getString(R.string.item_already_downloaded),
            positiveButton = getString(R.string.open),
            negativeButton = getString(R.string.cancel), positiveClickListener = { _, _ ->

                AppFileUtils.openFile(
                    requireContext(),
                    AppFileUtils.getFileUriFromFileName(
                        requireContext(),
                        AppFileUtils.getFileName(fileItem.url)
                    ),
                    AppFileUtils.getMimeTypeFromURL(fileItem.url) ?: ""
                )
            })
    }


    override fun observeOnUiState() {
        filesViewModel.getUiStateModelObservable().observe(viewLifecycleOwner, Observer {

            when (it) {
                is UiState.LOADING -> {
                    showLoading()
                }
                is UiState.RELOADING -> {
                    showReloading()
                }
                is UiState.SUCCESS -> {
                    showData()
                }
                is UiState.ERROR -> {
                    showError(it.message)
                }
                is UiState.NODATA -> {
                    showError(it.message)
                }
                is UiState.NOINTERNET -> {
                    showError(it.message)
                }
                UiState.IDLE -> {
                }
            }
        })
    }

    private fun isRefreshing(refreshing: Boolean) {
        swipe_refresh?.isRefreshing = refreshing
    }

    private fun showLoading() {
        isRefreshing(false)
        export_loading.show()
        rvFiles.hide()
        exportErrorView.hide()
    }

    private fun showReloading() {
        isRefreshing(true)
        export_loading.hide()
        rvFiles.hide()
        exportErrorView.hide()
    }

    private fun showData() {
        isRefreshing(false)
        export_loading.hide()
        rvFiles.fade()
        exportErrorView.hide()
    }

    private fun showError(message: Int) {
        isRefreshing(false)
        exportTvError.text = getString(message)
        export_loading.hide()
        rvFiles.hide()
        exportErrorView.fade()
    }

    override fun onPermissionGranted(permission: String) {
        when (permission) {
            Constants.PermissionTags.WRITE_EXTERNAL_STORAGE_TAG -> {
                showDownloadConfirmation()
            }
        }
    }

    private fun showDownloadConfirmation() {
        UIUtils.showBasicDialog(requireContext(),
            title = getString(R.string.download_confirm_title),
            positiveButton = getString(R.string.download),
            negativeButton = getString(R.string.cancel), positiveClickListener = { _, _ ->
                filesViewModel.downloadFiles()
            })
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        permissionHandler.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults
        )
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onNeverAskAgainChecked(permission: String) {
        showMessage(R.string.goToAppSettingForStorage)
    }

    override fun onPermissionDenied(permission: String) {
        when (permission) {
            Constants.PermissionTags.WRITE_EXTERNAL_STORAGE_TAG -> {
                showMessage(R.string.writeStoragePermissionDenied)
            }
        }
    }

    fun navigateUp() {
        if (filesViewModel.selectionState) filesViewModel.clearSelection()
        else activity?.onBackPressed()
    }


}