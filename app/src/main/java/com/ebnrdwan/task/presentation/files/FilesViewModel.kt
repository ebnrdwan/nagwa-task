package com.ebnrdwan.task.presentation.files

import android.app.Application
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ebnrdwan.core.data.error.manager.ErrorManager
import com.ebnrdwan.core.data.models.UiState
import com.ebnrdwan.core.util.Logger
import com.ebnrdwan.core.util.SingleLiveEvent
import com.ebnrdwan.corepresentation.base.BaseViewModel
import com.ebnrdwan.corepresentation.utils.NoInternetException
import com.ebnrdwan.filedownloader.errorhandler.IDownloadError
import com.ebnrdwan.filedownloader.factory.DownloadFactory
import com.ebnrdwan.filedownloader.factory.IDownloadFactory
import com.ebnrdwan.filedownloader.interactions.DownloadStatusCallback
import com.ebnrdwan.filedownloader.models.DownloadType
import com.ebnrdwan.task.R
import com.ebnrdwan.task.data.dto.files.FileItem
import com.ebnrdwan.task.data.dto.files.FilesResponse
import com.ebnrdwan.task.domain.downloadfiles.IDownloadFilesUseCase
import com.ebnrdwan.task.domain.fetchfiles.IFilesUseCase
import com.ebnrdwan.task.util.AppFileUtils
import com.ebnrdwan.task.util.Constants
import com.ebnrdwan.task.util.Constants.Ui.DEFAULT_SELECTION_COUNT
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class FilesViewModel @Inject constructor(
    private val application: Application,
    private val filesUseCase: IFilesUseCase,
    private val downloadFilesUseCase: IDownloadFilesUseCase
) : BaseViewModel() {
    @Inject
    lateinit var errorManager: ErrorManager
    var selectionState = false
    var downloadFactory: IDownloadFactory? =
        DownloadFactory(application.applicationContext).getInstance(DownloadType.DOWNLOAD_MANAGER)
    private var _selectedFiles: MutableList<FileItem>? = null
    private val _uiStatModel = MutableLiveData<UiState>().apply { }
    private val _fileList = MutableLiveData<List<FileItem>>()
    private val _needToUpdateSelectionState: MutableLiveData<Boolean> =
        MutableLiveData(selectionState)
    private val _selectedFilesCount: MutableLiveData<Int> = MutableLiveData(DEFAULT_SELECTION_COUNT)
    private val _toastMessages: SingleLiveEvent<String> = SingleLiveEvent()

    /*================ Export Immutable live data streams ================*/
    fun getFilesListObservable(): LiveData<List<FileItem>> = _fileList
    fun getUiStateModelObservable(): LiveData<UiState> = _uiStatModel
    fun getSelectedFilesCountObservable(): LiveData<Int> = _selectedFilesCount
    fun getShouldUpdateSelectionStateObservable(): LiveData<Boolean> = _needToUpdateSelectionState
    fun getToastMessagesObservable(): SingleLiveEvent<String> = _toastMessages


    fun loadFiles() = fetchFiles(UiState.LOADING)

    fun reloadFiles() {
        fetchFiles(UiState.RELOADING)
        clearSelection()
    }


    /*================fetch Files remotely-Start ================*/
    private fun fetchFiles(uiState: UiState) {
        addDisposable(
            filesUseCase.invoke()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    _uiStatModel.value = uiState
                }
                .subscribeFile()
        )
    }


    private fun Single<FilesResponse>.subscribeFile(): Disposable {
        return this.subscribeBy(
            onSuccess = { response ->
                onResponseSuccess(response)
            },
            onError = {
                onResponseError(it)
            }
        )
    }

    private fun onResponseError(it: Throwable) {
        Logger.log(message = "${Constants.Logging.RX_TAG_ERROR}: $it")
        if (it is NoInternetException)
            _uiStatModel.value = UiState.NOINTERNET
        else _uiStatModel.value = UiState.ERROR.apply {
            message = errorManager.getError(it).message
        }
    }

    private fun onResponseSuccess(response: FilesResponse) {
        if (response == null) _uiStatModel.value = UiState.NODATA
        else {
            emitFileListData(response)
        }
    }

    private fun emitFileListData(it: FilesResponse) {
        val filteredList = it.mapIndexed { index, fileItem ->
            fileItem.copy(downloaded = AppFileUtils.isFileExistBefore(application, fileItem.url))
        }
        _fileList.value = filteredList
        _uiStatModel.value = UiState.SUCCESS
    }


    /*================fetch Files remotely-End ================*/

    fun isFileAlreadyDownloaded(fileItem: FileItem): Boolean {
        return AppFileUtils.isFileExistBefore(application, fileItem.url)
    }

    fun setSelectedFile(fileItem: FileItem) {

        val currentList = _fileList.value?.toMutableList()
        currentList?.forEachIndexed { index, item ->
            item.takeIf { it.id == fileItem.id }?.let {
                currentList[index] = it.copy(selected = !it.selected)
            }
        }
        _fileList.postValue(currentList)
        _selectedFiles = currentList?.filter { it.selected }?.toMutableList()
        onSelectionChanged()
    }


    private fun setCurrentSelectionState(isSelectionOn: Boolean) {
        if (selectionState != isSelectionOn) {
            selectionState = isSelectionOn
            _needToUpdateSelectionState.postValue(true)
        }

    }

    private fun emitSelectedFilesCounter(isSelectionOn: Boolean) {
        if (isSelectionOn) _selectedFilesCount.postValue(_selectedFiles?.size)
    }

    fun clearSelection() {
        _fileList.postValue(_fileList.value?.map { it.copy(selected = false) }?.toMutableList())
        _selectedFiles?.clear()
        onSelectionChanged()
    }


    fun selectAllFiles() {
        _selectedFiles = _fileList.value?.map { it.copy(selected = !it.downloaded) }?.toMutableList()
        _fileList.postValue(_selectedFiles)
        onSelectionChanged()
    }

    private fun onSelectionChanged() {
        setCurrentSelectionState(!_selectedFiles.isNullOrEmpty())
        emitSelectedFilesCounter(!_selectedFiles.isNullOrEmpty())
    }


    private fun shouldDownloadFile(fileUrl: String?): Boolean {
        if (AppFileUtils.isInvalidUrl(fileUrl)) {
            emitToastMessage(R.string.invalid_file_link)
            return false
        }

        if (AppFileUtils.isInvalidExtension(fileUrl)) {
            emitToastMessage(R.string.file_type_not_supported)
            return false
        }

        if (AppFileUtils.isFileExistBefore(application, fileUrl!!)) {
            val fileName = AppFileUtils.getFileName(fileUrl)
            AppFileUtils.openFile(
                application,
                AppFileUtils.getFileUriFromFileName(application, fileName),
                AppFileUtils.getMimeTypeFromURL(fileUrl) ?: ""
            )
            return false
        }
        return true
    }

    private fun emitToastMessage(@StringRes messageRes: Int) {
        _toastMessages.postValue(application.getString(messageRes))
    }

    fun downloadFiles(
    ) {
        emitToastMessage(R.string.downloading)
        _selectedFiles?.forEach { file ->
            if (shouldDownloadFile(file.url))
                downloadFilesUseCase(
                    downloadFactory!!,
                    application.applicationContext,
                    file.url,
                    true,
                    downloadStatus
                )
        }
        clearSelection()
    }


    private val downloadStatus = object :
        DownloadStatusCallback {
        val TAG = "Status_TAG"
        override fun pending(downloadId: Long) {

        }

        override fun onProgress(downloadId: Long, downloadedBytes: Long, totalBytes: Long) {
            // TODO: 6/7/2021 set progress in item layout if you wish
            Logger.log(tag = TAG, message = "progress $downloadedBytes ")
        }

        override fun onPause(downloadId: Long, reasonCode: Int) {}
        override fun onCompleted(downloadId: Long, contentPath: String, absolutePath: String) {
            fetchFiles(UiState.IDLE)
        }

        override fun onCanceled(downloadId: Long) {}
        override fun onError(downloadId: Long, downloadManagerError: IDownloadError) {}
    }
}