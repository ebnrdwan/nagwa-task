package com.ebnrdwan.task.di

import com.ebnrdwan.core.di.FragmentScope
import com.ebnrdwan.task.di.files.FilesModule
import com.ebnrdwan.task.ui.currencies.FileListFragmentTest
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = [FilesModule::class])
interface TestCurrenciesSubcomponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): TestCurrenciesSubcomponent
    }

    fun inject(currencyListFragmentTest: FileListFragmentTest)
}