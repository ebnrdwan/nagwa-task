package com.ebnrdwan.corepresentation.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {
    open fun observeOnUiState() {}
    abstract fun getLayout(): Int
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(getLayout(), container, false)


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
        setListeners()
        bindOnViewModel()
    }

    protected fun showMessage(message: Any) {
        when (message) {
            is String -> showToast(requireContext(), message)
            is Int -> showToast(requireContext(), getString(message))
        }
    }

    private fun showToast(mContext: Context, string: String) {
        Toast.makeText(mContext, string, Toast.LENGTH_SHORT).show()
    }

    abstract fun bindOnViewModel()

    abstract fun setListeners()

    abstract fun initViews()

}